import * as d3 from "d3";

import { legendColor } from "d3-svg-legend";

const svgDimensions = { width: 800, height: 500 };
const margin = { left: 5, right: 5, top: 10, bottom: 10 };
const chartDimensions = {
  width: svgDimensions.width - margin.left - margin.right,
  height: svgDimensions.height - margin.bottom - margin.top
};

const file = require('./data/mcd.json');
// console.log(file);

// get the element you want to add the button to
var myDiv = document.getElementById("buttons");

Object.keys(file).forEach(k => {
  // create the button object and add the text to it
  var button = document.createElement("BUTTON");
  button.innerHTML = k;
  button.addEventListener("click", function handleResultsNovember() {
    updateChart(file[k]);
  });
  myDiv.appendChild(button);
})


// add the button to the div


// Smoother Transition: https://bl.ocks.org/tezzutezzu/c2653d42ffb4ecc01ffe2d6c97b2ee5e
// Version 1
const updateChart = (data: any[]) => {
  const politicalPartiesKeys: string[] = data.map(d => d.word);
  const dataChart = data.map(d => {
    return {
      word: d.word,
      ocurrences: d.data.ocurrences
    }
  })
  const partiesColor = [
    "#ED1D25",
    "#0056A8",
    "#5BC035",
    "#6B2E68",
    "#F3B219",
    "#FA5000",
    "#C50048",
    "#029626",
    "#A3C940",
    "#0DDEC5",
    "#FFF203",
    "#FFDB1B",
    "#E61C13",
    "#73B1E6",
    "#BECD48",
    "#017252",
    "#DD0000"
  ];
  var myDiv = document.getElementById("chart");
  myDiv.innerHTML = "";

  var ordinal = d3
    .scaleOrdinal()
    .domain(politicalPartiesKeys)
    .range(partiesColor);
  
  const svg = d3
    .select("#chart")
    .append("svg")
    .attr("width", svgDimensions.width)
    .attr("height", svgDimensions.height)
    .attr("style", "background-color: #FBFAF0");
  
  const chartGroup = svg
    .append("g")
    .attr("transform", `translate(${margin.left}, ${margin.top})`)
    .attr("width", chartDimensions.width)
    .attr("height", chartDimensions.height);
  
  const radius = Math.min(chartDimensions.width, chartDimensions.height) / 2;
  
  chartGroup.attr("transform", `translate(${radius},${radius})`);
  
  const arc = d3
    .arc()
    .innerRadius(radius / 1.7) // We want to have an arc with a propotional width
    .outerRadius(radius);
  
  const pieChart = d3
    .pie()
    .startAngle(-90 * (Math.PI / 180))
    .endAngle(90 * (Math.PI / 180))
    .value(d => d["ocurrences"])
    .sort(null);
  
  const pie = pieChart(<any>dataChart);
  
  const arcs = chartGroup
    .selectAll("slice")
    .data(pie)
    .enter();
  
  arcs
    .append("path")
    .attr("d", <any>arc) // Hack typing: https://stackoverflow.com/questions/35413072/compilation-errors-when-drawing-a-piechart-using-d3-js-typescript-and-angular/38021825
    .attr("fill", (d, i) => partiesColor[i]); // TODO color ordinal
  
  // Legend
  
  var legendOrdinal = legendColor().scale(ordinal);
  
  const legendLeft = margin.left;
  const legendTop = radius + 5;
  
  const legendGroup = svg
    .append("g")
    .attr("transform", `translate(${legendLeft},${legendTop})`);
  
  legendGroup.call(legendOrdinal);

  var myDiv2 = document.getElementById("info");
  myDiv2.innerHTML = "";
  var ul = document.createElement("ul");
  data.forEach(d => {
    var li = document.createElement("li");
    li.innerHTML = `Word: <b>${d.word}</b> (${d.data.ocurrences}) Sources: ${d.data.sources}`;
    ul.appendChild(li);
  });
  myDiv2.appendChild(ul);
};
