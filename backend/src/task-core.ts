import { ImagesEndpoint } from "./core/images-endpoint";
import { InferenceMcd } from "./core/inference-mcd";
import { WordsAnalizer } from "./core/words-analyzer";


export class TaskCore {

    async launchTask1() {

        console.log("Start Task 1");
        const endpoint = new ImagesEndpoint();
        if (await endpoint.createOcrDataFile()) {
            console.log("Done Task 1");
        };

    }

    async launchTask2() {

        console.log("Start Task 2");
        const analyzer = new WordsAnalizer();
        if (await analyzer.generateWordsFile()) {
            console.log("Done Task 2");
        };

    }

    async launchTask3() {

        console.log("Start Task 3");
        const inferencer = new InferenceMcd();
        if (await inferencer.generateMcdFile()) {
            console.log("Done Task 3");
        };

    }
}
