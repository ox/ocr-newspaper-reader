import { delayMillis } from "./logic/utils";
import tesseract from "node-tesseract-ocr";
import fs from 'fs';
import { CommonWords } from "./logic/common-words";
import { myOcrData } from "./logic/types";

export class ImagesEndpoint {

    /**
     * Contents to write at file ocr-data.json
     */
    results: myOcrData = {};

    private common = new CommonWords();

    /**
     * Consider that at url this.baseUrl...
     *
     * ... we can loop with an iterator (2 positions filling zero)
     * ... this iterator is the "day" or "iteration".
     *
     * For each day we will read all its channels (will get an image)...
     * ... and will extract its text (ocr parsing).
     *
     * So final url to find the image is:
     *
     * - this.baseUrl, indexAsIteratorKey( 2), "/" + channel
     *
     * Example:
     *
     *  - https://lasportadas.es/data/2021/20211005/GLaRazon.jpg}
     *
     *  @returns Will write to disk "ocr_data.json" file.
     */
    async createOcrDataFile(): Promise<boolean> {

        for(const channel of this.common.getImageChannelsList()) {
            for(let i = 1; i < this.common.MAX_ITERATIONS; i++) {
                await this.applyTesseractReading(
                    this.common.baseUrl, this.common.indexAsIteratorKey(i), "/" + channel);
            }
        }
        return true;

    }

    private async applyTesseractReading(baseUrl: string, iteration: string, key: string) {

        // TODO export this configuratin options
        const config = {
            lang: "eng",
            oem: 1,
            psm: 3,
        };
        const url = baseUrl + iteration + key;
        console.log(new Date(), "Reading for:", url, config);
        await delayMillis(1000);

        tesseract
            .recognize(url, config)
            .then((text) => {
                if (!this.results[key]) {
                    this.results[key] = [];
                }
                this.results[key].push({
                    key,
                    baseUrl,
                    iteration,
                    text
                });
                let json = JSON.stringify(this.results);
                fs.writeFileSync(this.common.OCR_DATA_FILE, json);
            })
            .catch((error) => {
                console.log(error.message);
            });
    }
}