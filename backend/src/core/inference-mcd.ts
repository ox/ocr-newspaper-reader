import fs from 'fs';
import { CommonWords } from './logic/common-words';
import { myIteration, myReducedWord, myWord, myMCD } from './logic/types';
import { groupBy } from './logic/utils';

/**
 * Retrieves a list of "days" or "iterations"...
 *
 * ... for each one, it contains the most used words with:
 *  - word
 *  - number of times used
 *  - channel images where the word was found.
 */
export class InferenceMcd {

    mcdWords: myMCD = {};

    private common = new CommonWords();

    /**
     * Create a file ready for the frontend to be loaded on UI
     *
     * @returns The file with: "For each day or iteration, the most used words
     *  in its channels"
     */
    generateMcdFile(): boolean {

        let channelList: myWord;
        const data = fs.readFileSync(this.common.WORD_DATA_FILE, 'utf8');

        if (data){
          channelList = JSON.parse(data);

          for(let iteration = 1; iteration < this.common.MAX_ITERATIONS; iteration++) {

            console.log("Working iteration", iteration);
            // Extract for each "day" or "iteration" all its words...
            const iterationList = this.reduceWordsInIteration(iteration, channelList);
            const itAsKey = this.common.indexAsIteratorKey(iteration);
            console.log(">> ", (iterationList[itAsKey] || []).length, "words.");
            // apply the mcd logic to get the "common words between the channels"
            this.appendWordsToMcdResults(iterationList);

            // write file
            let json = JSON.stringify(this.mcdWords);
            fs.writeFileSync(this.common.MCD_FILE, json);
            // console.log(mcdWords)
          }

        } else {
          console.log("No data");
        }
        return true;
    }

    private reduceWordsInIteration(iteration: number, source: myWord): myMCD {

        const itAsKey = this.common.indexAsIteratorKey(iteration);

        const wordsWithOcurrence = Object
            .keys(source)
            .filter(word => Object.keys(source[word]).indexOf(itAsKey) > -1)
            .map(word => {
                const mw: myIteration = source[word];
                return {
                    word, itAsKey, data: mw[itAsKey]
                }
            });
        return groupBy(wordsWithOcurrence, o => o.itAsKey);
    }

    private appendWordsToMcdResults(result: myMCD) {

        // For each iteration...
        for(const iteration of Object.keys(result)) {

            // Sort the words by its ocurrence number
            const words = result[iteration];
            words.sort((a: myReducedWord, b: myReducedWord) =>
                a.data.ocurrences < b.data.ocurrences ? 1 : -1)
            const top = words.length;

            // Take the top words and add to results.
            let foundWords = this.common.NUMBER_OF_WORD_IN_THE_TOP;
            for(let wordIndex = 0; wordIndex < top; wordIndex++) {
                const added = this.addWord(iteration, wordIndex, words);
                if (added) foundWords--;
                if (foundWords == 0) break;
            }
        }
    }

    /**
     * Adds a word to the top list
     *
     * @param iteration
     * @param index
     * @param words
     * @param foundWords
     */
    private addWord(iteration: string, index: number,
        words: myReducedWord[]): boolean {

      try {
          const asnumber = parseInt(words[index].word);
          if (asnumber) {
            // pass
          } else {
            if (words[index].word.length > 2) {
              /*console.log(
                "Iteration", iteration,
                "Word", words[realCounter].word,
                "Ocurrences", words[realCounter].data.ocurrences,
                "Sources", words[realCounter].data.sources);*/
              if (!this.mcdWords[iteration]) {
                this.mcdWords[iteration] = [];
              }
              this.mcdWords[iteration].push(words[index]);
              return true;
            }
          }
        } catch(ex) {
          console.log(ex)
        }

        return false;

    }
}
