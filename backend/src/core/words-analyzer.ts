import fs from 'fs';
import { AllowedWords } from './logic/allowed-words';
import { CommonWords } from './logic/common-words';
import { myOcrData, ShotData } from './logic/types';
import { ShortParser } from './logic/shot-parser';

export class WordsAnalizer {

    private common = new CommonWords();

    async generateWordsFile(): Promise<boolean> {
        const file = this.common.OCR_DATA_FILE;
        const data = fs.readFileSync(file, 'utf8');
        if (data){

          // Read the file
          let source: myOcrData;
          source = JSON.parse(data);
          const shotParser = new ShortParser();
          // For each "day" or "iteration"...
          Object.keys(source).forEach(async d => {

            // console.log("Analice:", d, "Number of items", source[d].length);
            await this.addWordsToWordsFile(source[d], shotParser);

          });
        } else {
          console.log("No data");
        }

        return true;
    }

    private async addWordsToWordsFile(data: ShotData[], shotParser: ShortParser): Promise<boolean> {

        const allowed = new AllowedWords();

        if (data) {

          for(const shot of data) {

            // Extract shot data
            shot.text = shot.text || "";
            // console.log(shot.key, shot.iteration, "Text length:", shot.text.length);

            // Split the given data by lines...
            const lines = shot.text.split("\n") || [];
            for(const line of lines) {

              if (!line) continue;

              // Split the lines into words
              const words = line.split(" ") || [];
              // console.log(shot.key, shot.iteration, "Lineas:", lines.length, "Palabras:", words.length);
              for(let word of words) {

                if (!allowed.isAllowed(word)) continue;

                shotParser.parseShot(shot, word);
              }
            }
          }
          // Save result to file
          let json = JSON.stringify(shotParser.wordsUnique);
          fs.writeFileSync(this.common.WORD_DATA_FILE, json);
        } else {
          console.log("No screenshot data found!");
        }
        return true;
    }
}