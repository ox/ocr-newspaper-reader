
export interface myLeaf { ocurrences: number; sources: string[]; }

export interface myIteration { [iteration: string]: myLeaf; }

export interface myReducedWord {
    word: string;
    itAsKey: string;
    data: myLeaf;
}
export interface myMCD { [iteration: string]: myReducedWord[]; }

export interface myWord { [channel: string]: myIteration; }

export interface myOcrData { [source: string]: ShotData[]; }

export interface ShotData {
    key: string;
    baseUrl: string;
    iteration: string;
    text: string;
}
