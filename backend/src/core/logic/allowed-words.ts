import { CommonWords } from "./common-words";

export class AllowedWords {
    commonWords = new CommonWords();

    isAllowed(word: string): boolean {
        if (!word) return false;
        if (parseInt(word)) return false;
        if (word.length < 3) return false;
        if (this.commonWords.contains(word))  return false;
        return true;
    }
}