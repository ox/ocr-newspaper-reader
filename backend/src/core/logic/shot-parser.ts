import { myWord, ShotData } from "./types";

export class ShortParser {

    wordsUnique: myWord = {};

    parseShot(shot: ShotData, word: string): void {

        word = word.toLocaleLowerCase();
        // console.log("The word", word)
        word = (word + "").replace(/ /g, '_');
        // console.log("Before Word:", word, this.wordsUnique[word]);
        let ocurrences = 1;
        let sources: string[] = []
        if (!this.wordsUnique[word]) {
          this.wordsUnique[word] = {};
        }
        let leaf = this.wordsUnique[word][shot.iteration];
        if (!leaf) {
          leaf = {
            ocurrences: 0,
            sources: []
          };
        }
        ocurrences = leaf.ocurrences + 1;

        sources = leaf.sources;
        sources.push(shot.key);

        // console.log(">> Word", word, "shot.iteration", shot.iteration)
        this.wordsUnique[word][shot.iteration] = {
            ocurrences,
            sources,
          };
    }
}