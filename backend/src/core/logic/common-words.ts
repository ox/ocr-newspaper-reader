import { pad } from "./utils";

export class CommonWords {

    pathToBucket = '/../../bucket/';
    /**
     * This file will contain a myOcrData structure.
     *
     * Meaning:
     *
     * For each iteration/channel, its OCR text.
     */
    OCR_DATA_FILE: string = __dirname + this.pathToBucket + 'ocr_data.json';

    /**
     * This file will contain a myWord structure.
     *
     * Meaning:
     *
     * For each word, the iterations/channels where it appears.
     */
    WORD_DATA_FILE: string = __dirname + this.pathToBucket + 'word.json';

    /**
     * This file will contain a myWordsWithOcurrence structure.
     *
     * Meaning:
     *
     * For each iteration, the top list of most used words and the
     * channels where they appear.
     */
    MCD_FILE: string = __dirname + this.pathToBucket + 'mcd.json';

    /**
     * This a list of common words that we will filter, avoiding them
     * to appear in the top list of most used words.
     */
    commonWords = ['eee', 'hoy', 'ayer', 'mañana', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve', 'diez',
        'ancho', 'casi', 'esta', 'estos', 'enero', 'febrero','marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto',
        'septiembre', 'octubre', 'noviembre', 'diciembre', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado',
        'domingo', 'no', 'antes', 'res', '«no', 'si', 'sí', 'cada', 'queda', 'fin', '',  'pero', 'primera', 'segunda', 'tercera', 'cuarta',
        'a', 'ante', 'bajo', 'cabe', 'con', 'contra', 'de', 'del', 'un', 'una', 'unos', 'unas', 'desde', 'hasta', 'para',
        'por', 'sin', 'so', 'son', 'sobre', 'tras', 'mas', 'más', 'falta', 'ala', 'sus', 'ese', 'esa', 'esos', 'esas', 'entre',
        'sobra', 'dela', 'el', 'la', 'los', 'las', 'que', 'y', 'o', 'él', 'yo', 'nosotros', 'es', 'son', 'hay', 'han', 'haya', 'he',
        'como', 'cómo', 'con', 'dea', 'grandes', 'pequeñas', 'entra', 'hazlo', 'tiene', 'dea', 'solo', 'sólo', '|p.', '"el' ];

    /**
     * This list is images endpoint dependant.
     *
     * For each iteration, it is assumed that all this channels
     * are proved.
     */
    imageChannels = ["GABCI.jpg", "GLaRazonI.jpg", "GElPaisI.jpg", "GElMundoI.jpg", "GLaVanguardiaI.jpg",
        "GLaVozdeGaliciaI.jpg", "GDiariodeSevillaI.jpg", "GPeriodicoCatalunyaCASI.jpg", "GElDiarioVascoI.jpg"];

    /**
     * Main images source endpoint url
     */
    baseUrl = "https://lasportadas.es/data/2021/202110";

    /**
     * The number of "days" or "iterations" for a source.
     * In this example, we will consider a month of 30 days.
     */
    MAX_ITERATIONS = 30;

    /**
     * For each "day" & channel with pick this number of top words.
     */
    NUMBER_OF_WORD_IN_THE_TOP: number = 20;

    /**
     * Use to loop over the iteration field by applying transformations
     * relative to the image endpoint custom rules.
     * 
     * @param i 
     * @returns 
     */
    indexAsIteratorKey(i: number): string {
        return pad(i, 2);
    }

    /**
     * 
     * @returns The common words list that should be not included
     * in the top list.
     */
    getWords(): string[] {
        return this.commonWords;
    }

    contains(word: string): boolean {
        return this.commonWords.indexOf(word.toLocaleLowerCase()) > -1
    }

    /**
     * A channel represents an ocurrence inside a "day" or "iteration".
     *
     * In this example every day has a set of channels meaning the newspapers 
     * of this day.
     *
     * @returns
     */
    getImageChannelsList() {
        return this.imageChannels;
    }
}