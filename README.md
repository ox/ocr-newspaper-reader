# What is this project?

## Project precondition
The seed for this repo was a newspaper-front-pages site, such [lasportadas.es](https://lasportadas.es/).

![](pics/lasportadas.png)

There is a big-data waiting for projects...

Taking a closest look to this site:

Url for resources:

- https://lasportadas.es/d/20211008
- https://lasportadas.es/data/2021/20211008/GABCI.jpg
- https://lasportadas.es/data/2021/20211008/GLaRazonI.jpg

That means:
- https://{baseUrl}/{year}/{year}{month}{day}/{newspaper_id}.jpg


So, that was the project precondition. Create some utility to take benefit from this source of data.

## Project definition
The goal of the project will be to create a tool for parsing each newspaper frontpage with ocr and get a "top list" (most used words) that reduces everyday.

Project will have 2 axis:
- **backend**: will read the images, get the texts and build dictionaries.
- **frontend**: will read dictionaries and show data on charts.

See in this picture:

- top: buttons meaning days of month "november".
- arch: top used words in the same day by distinct newspapers frontpages.  

![](pics/ui.png)

## Details

See types at [types, interfaces](backend/src/core/logic/types.ts) to the model domain.

See produced dictionary files at [bucket](backend/src/bucket):

- **ocr_data.json**: Results from task 1, reading the images and ocr to text.
- **words.json**: Results from task 2, using ocr_data.json to get a list of unique words grouping by its ocurrences.
- **mcd.json**: use words.json to make a ranking with top words for each day.

Most configurable things are at [common-words.ts](backend/src/core/logic/common-words.ts).

## Considerations
- There is an array of words to be discarted at [common-words.ts](backend/src/core/logic/common-words.ts).
- There is a list of newspaper to be checked "imageChannels" at [common-words.ts](backend/src/core/logic/common-words.ts).


# What tech uses?
This repo contains the [frontend](./frontend) and the [backend](./backend) for an ["node-tesseract-ocr": "^2.2.1"](https://www.npmjs.com/package/node-tesseract-ocr) based project.

It uses simple nodejs-ts [backend boilerplate](https://github.com/metachris/typescript-boilerplate) and simple d3js-parcel [frontend boilerplate](https://github.com/Lemoncode/d3js-typescript-examples/tree/master/01-from-barchart-to-semi-arch/05-multiple-series).



